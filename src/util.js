const getInt = value => +value.toString(10);

export { getInt };
