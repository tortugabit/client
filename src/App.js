import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'react-final-form';
import { Route, Switch, Link } from 'react-router-dom';

import { withWeb3 } from './hoc';
import Header from './components/Header';
import Home from './pages/Home';
import User from './pages/User';
import { Container } from './components/Grid';

const OWNER_ETH_ID = '0x1Cc58444AAee4f257cB410A9383B6860cbab6cb2';
class App extends Component {
  static propTypes = {
    contracInstance: PropTypes.object
  };

  state = {
    isSuccess: false,
    isFail: false
  };

  onSubmit = ({ name = 'test name', quantity = 432, skin: url }) => {
    const { contracInstance } = this.props;

    contracInstance.getCollectionsIdByOwner(OWNER_ETH_ID, (err, result) => {
      if (err) {
        this.setState({ isFail: true });
        console.log(err);
      }

      console.log(result);

      const id = +result[result.length - 1].toString(10);

      // TODO
      contracInstance.addEntity(0, 'default', url, (err, result) => {
        if (err) console.log(err);

        console.log(result);
      });
    });
  };

  render() {
    const { isSuccess, isFail } = this.state;

    return (
      <Fragment>
        <Header isSuccess={isSuccess} />
        <Container>
          <Switch>
            <Route
              exact
              path="/"
              render={() => {
                return (
                  <Home
                    isSuccess={isSuccess}
                    isFail={isFail}
                    onSubmit={this.onSubmit}
                  />
                );
              }}
            />
            <Route
              path="/user/:id"
              render={props => {
                return <User {...props} />;
              }}
            />
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

export default withWeb3(App);
