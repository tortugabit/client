import React from 'react';
import styled from 'styled-components';

import { Container, Row, Column } from './Grid';
import background from '../background.png';

const Wrapper = styled('div')`
  background-color: #752525;
  background-size: cover;
  background-position: top;
  background-image: url(${background});
  height: 400px;
  margin-bottom: 80px;

  h1 {
    color: white;
    font-size: 56px;
  }
`;

const Logo = styled('div')`
  font-size: 80px;
  color: #611919;
  padding-top: 16px;
  font-weight: 700;
`;

const Header = ({ isSuccess }) => {
  const title = isSuccess ? 'Result' : 'New collection';
  return (
    <Wrapper>
      <Container>
        <Row>
          <Column>
            <Logo>Tortuga</Logo>
          </Column>
        </Row>
      </Container>
    </Wrapper>
  );
};

export default Header;
