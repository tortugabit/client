import React from 'react';
import { Container, Row, Column } from './Grid';

const RequestStatus = ({ isFail }) => {
  const gif = isFail
    ? 'https://media.giphy.com/media/wkgQR3TQqc0x2/giphy.gif'
    : 'https://media.giphy.com/media/j5QcmXoFWl4Q0/giphy.gif';
  return (
    <Container>
      <Row justifyContent="center">
        <Column w={[1, , 3 / 4]} flex="0 1 auto" textAlign="center">
          <img src={gif} alt="" />
        </Column>
      </Row>
    </Container>
  );
};

export default RequestStatus;
