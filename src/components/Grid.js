import React from 'react';
import styled from 'styled-components';
import { textAlign } from 'styled-system';
import { Flex, Box } from 'grid-styled';

const Container = styled(Box)`
  max-width: 1152px;
`;
Container.defaultProps = {
  mx: 'auto',
  px: [1, , 2]
};
const Row = props => <Flex {...props} mx={[-1, , -2]} />;
const ColumnBase = props => <Box flex="1 1 auto" {...props} px={[1, , 2]} />;

const Column = styled(ColumnBase)`
  ${textAlign}
`;

export { Container, Row, Column };
