import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Form } from 'react-final-form';
import arrayMutators from 'final-form-arrays';

import { Row, Column } from './Grid';
import RequestStatus from './RequestStatus';

const ButtonPrev = styled('button')`
  background-color: #9849ef;
`;

class Wizard extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    initialValues: PropTypes.object,
    isSuccess: PropTypes.bool,
    isFail: PropTypes.bool
  };
  static Page = ({ children, ...props }) =>
    typeof children === 'function' ? children(props) : children;

  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      values: props.initialValues || {}
    };
  }
  next = values =>
    this.setState(state => ({
      page: Math.min(state.page + 1, this.props.children.length - 1),
      values
    }));

  previous = () =>
    this.setState(state => ({
      page: Math.max(state.page - 1, 0)
    }));

  validate = values => {
    const activePage = React.Children.toArray(this.props.children)[
      this.state.page
    ];
    return activePage.props.validate ? activePage.props.validate(values) : {};
  };

  handleSubmit = values => {
    const { children, onSubmit } = this.props;
    const { page } = this.state;
    const isLastPage = page === React.Children.count(children) - 1;
    if (isLastPage) {
      return onSubmit(values);
    } else {
      this.next(values);
    }
  };

  renderActivePage(children, page) {
    const activePage = React.Children.toArray(children)[page];
    return props => {
      return typeof activePage.props.children === 'function'
        ? activePage.props.children(props)
        : activePage;
    };
  }

  render() {
    const { children, isSuccess, isFail } = this.props;
    const { page, values } = this.state;
    const getActivePage = this.renderActivePage(children, page);
    const isLastPage = page === React.Children.count(children) - 1;
    return isSuccess ? (
      <RequestStatus isFail={isFail} />
    ) : (
      <Form
        initialValues={values}
        mutators={{ ...arrayMutators }}
        validate={this.validate}
        onSubmit={this.handleSubmit}
      >
        {({ handleSubmit, submitting, mutators: { push, pop }, values }) => (
          <form onSubmit={handleSubmit}>
            <Row justifyContent="center">
              <Column w={[1, , 3 / 4]} flex="0 1 auto">
                <Row mb={32}>
                  <Column>{getActivePage({ push, pop })}</Column>
                </Row>
                <Row>
                  <Column w={[1, , 3 / 4]}>
                    <Row>
                      <Column w={[1, 1 / 2]}>
                        {page > 0 && (
                          <ButtonPrev type="button" onClick={this.previous}>
                            « Previous
                          </ButtonPrev>
                        )}
                      </Column>
                      <Column w={[1, 1 / 2]} textAlign="right">
                        {!isLastPage && <button type="submit">Next »</button>}
                        {isLastPage && (
                          <button type="submit" disabled={submitting}>
                            Submit
                          </button>
                        )}
                      </Column>
                    </Row>
                  </Column>
                </Row>
              </Column>
            </Row>
          </form>
        )}
      </Form>
    );
  }
}

export default Wizard;
