import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'react-final-form';

import { Row, Column } from '../components/Grid';
import Wizard from '../components/Wizard';

const SKIN_URL =
  'https://pbs.twimg.com/profile_images/661080596886781952/rLQR4VJl_400x400.png';

const propTypes = {
  isSuccess: PropTypes.bool,
  isFail: PropTypes.bool,
  onSubmit: PropTypes.func
};

const Home = ({ isSuccess, isFail, onSubmit }) => {
  return (
    <Wizard
      isSuccess={isSuccess}
      isFail={isFail}
      onSubmit={onSubmit}
      initialValues={{
        skin: SKIN_URL
      }}
    >
      <Wizard.Page>
        <Field
          name="name"
          component="input"
          type="text"
          placeholder="collection name"
        />
      </Wizard.Page>
      <Wizard.Page>
        <Field
          name="quantity"
          component="input"
          type="number"
          placeholder="quantity"
        />
      </Wizard.Page>
      <Wizard.Page>
        <Field name="skin" component="input" type="text" />
      </Wizard.Page>
    </Wizard>
  );
};

Home.propTypes = propTypes;

export default Home;
