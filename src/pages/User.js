import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { getInt } from '../util';
import { withWeb3 } from '../hoc';
import { Row, Column } from '../components/Grid';
import noUserImg from '../no-user.png';

const USER_COLLECTION = {
  COLLECTION_NAME: 0,
  ACTION_NAME: 1,
  META: 2,
  COLLECTION_COUNT: 3,
  AUTOR: 4
};

const UserAvatar = styled('img')`
  height: 100px;
  max-width: 100%;
`;

const ThumbNail = styled('img')`
  height: 200px;
  max-width: 100%;
`;

class User extends Component {
  static propTypes = {
    contracInstance: PropTypes.object,
    match: PropTypes.object
  };

  state = {
    collections: []
  };

  componentDidMount() {
    const { contracInstance, match } = this.props;
    const {
      params: { id: address }
    } = match;

    // contracInstance.setEntityOwner(address, 'default', (err, result) => {
    //   console.log(result);
    // })
    contracInstance.getCollectionsIdByOwner(address, (err, collectionsId) => {
      collectionsId.forEach(collection => {
        contracInstance.getEntitiesIdByOwner(
          address,
          collection,
          (err, entitiesId) => {
            if (err) console.log(err);

            entitiesId.forEach((entity, i) => {
              const entityId = getInt(entity);
              const collectionId = getInt(collection);

              contracInstance.getCollectionEntity(
                collectionId,
                entityId,
                (err, result) => {
                  if (err) console.log(err);
                  // console.log(result);

                  this.setState(prevState => {
                    return {
                      collections: [
                        ...prevState.collections,
                        {
                          name: result[USER_COLLECTION['COLLECTION_NAME']],
                          action: result[USER_COLLECTION['ACTION_NAME']],
                          meta: result[USER_COLLECTION['META']],
                          count: getInt(
                            result[USER_COLLECTION['COLLECTION_COUNT']]
                          ),
                          autor: result[USER_COLLECTION['AUTOR']]
                        }
                      ]
                    };
                  });
                }
              );
            });
          }
        );
      });
    });
  }

  render() {
    const { collections } = this.state;
    const {
      params: { id: address }
    } = this.props.match;
    return (
      <Row>
        <Column>
          <Row mb={3}>
            <Column textAlign="center">
              <UserAvatar src={noUserImg} alt="" />
              <h2>User #{address}</h2>
            </Column>
          </Row>
          <Row flexWrap="wrap" alignItems="center">
            {collections.map((entity, i) => {
              return (
                <Column w={[6 / 12]} key={i} textAlign="center">
                  <ThumbNail src={entity.meta} alt="" />
                  <h3>Collection name: {entity.name}</h3>
                </Column>
              );
            })}
          </Row>
        </Column>
      </Row>
    );
  }
}

export default withWeb3(User);
