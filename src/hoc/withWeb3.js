import React, { Component } from 'react';
import abi from '../TortugaTokenAbi.json';

const TOKEN_ADDRESS = '0x3801ea70c661922f742a1355137382ac646ebd91';

// TODO use context
const withWeb3 = WrappedComponent => {
  class Wrapped extends Component {
    // TODO
    componentWillMount() {
      if (typeof window.web3 !== 'undefined') {
        const { web3 } = window;
        const contract = web3.eth.contract(abi);
        this.contracInstance = contract.at(TOKEN_ADDRESS);
      } else {
        console.log('No web3? You should consider trying MetaMask!');
      }
    }

    render() {
      return (
        <WrappedComponent
          contracInstance={this.contracInstance}
          {...this.props}
        />
      );
    }
  }

  Wrapped.displayName = `withWeb3(${WrappedComponent.displayName ||
    WrappedComponent.name ||
    'Component'})`;

  return Wrapped;
};

export default withWeb3;
